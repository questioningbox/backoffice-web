import { Stack } from "@mui/material";
import Typography from "@mui/material/Typography";
import React, { useEffect, useState } from "react";
import { AiOutlineCloudUpload } from "react-icons/ai";
import { Row } from "read-excel-file";
import { useAppDispatch, useAppSelector } from "../../../app/hooks";
import { CustomIconButton, NoDataView } from "../../../components";
import { FeaturesThunk, QuestionsThunk } from "../../../functions";
import QuestionsModel, {
  CreateQuestionDto,
  QuestionsUploadDto,
} from "../../../model/QuestionsModel";
import ApiRoutes from "../../../routes/ApiRoutes";
import { UploadQuestionsModal } from "../../../views";
import { QuestionInfoCard } from "../components";
import { prepareQuestionData } from "../services";

export default function QuestionsPage() {
  const [uploadQuestions, setUploadQuestions] = useState<boolean>(false);
  const [uploadInfo, setUploadInfo] = useState<QuestionsUploadDto>({
    questions: [],
    category: "",
    academicLevel: [],
  });
  const { user } = useAppSelector((state) => state.UserReducer);
  const dispatch = useAppDispatch();
  const { questions } = useAppSelector((state) => state.QuestionsReducer);

  ///
  function handleQuestionsData(rows: Row[]) {
    // console.log(rows);
    const data = rows.map(prepareQuestionData);
    setUploadInfo({ ...uploadInfo, questions: data });
  }

  function handleFeatures() {
    dispatch(
      FeaturesThunk({
        token: user?.token,
        url: ApiRoutes.category.get,
        method: "get",
      })
    );
  }

  function handleQuestionsUpload() {
    dispatch(
      QuestionsThunk({
        data: uploadInfo,
        method: "post",
        token: user?.token,
        url: ApiRoutes.questions.crud,
      })
    );
    setUploadInfo({
      questions: [],
      category: "",
      academicLevel: [],
    });
  }

  function getQuestions() {
    dispatch(
      QuestionsThunk({
        method: "get",
        url: ApiRoutes.questions.crud,
        token: user?.token,
        params: {
          page: questions.page,
          pageSize: 10,
        },
      })
    );
  }

  useEffect(() => {
    getQuestions();
    handleFeatures();
  }, []);
  return (
    <Stack padding={2} width="100%">
      <Stack
        padding={1}
        direction="row"
        width="100%"
        alignItems="center"
        justifyContent="space-between"
        bgcolor={(theme) => theme.palette.common.white}
        borderRadius={(theme) => theme.spacing(0.85)}
      >
        <UploadQuestionsModal
          questions={uploadInfo.questions}
          handleRows={handleQuestionsData}
          handleUpload={handleQuestionsUpload}
          open={uploadQuestions}
          handleClose={() => setUploadQuestions(false)}
          handleCategory={(cate) =>
            setUploadInfo({ ...uploadInfo, category: cate })
          }
          handleAcademicLevel={(level) => {
            if (uploadInfo.academicLevel.includes(level)) {
              setUploadInfo({
                ...uploadInfo,
                academicLevel: uploadInfo.academicLevel.filter(
                  (l) => l !== level
                ),
              });
            } else {
              setUploadInfo({
                ...uploadInfo,
                academicLevel: [...uploadInfo.academicLevel, level],
              });
            }
          }}
          uploadInfo={uploadInfo}
        />
        <Stack direction="row" alignItems="center" justifyContent="flex-start">
          <Typography variant="body1">Questions</Typography>
        </Stack>
        <Stack
          direction="row"
          alignItems="center"
          justifyContent="flex-end"
          spacing={1}
        >
          <CustomIconButton
            props={{
              color: "primary",
              sx: (theme) => ({
                bgcolor: theme.palette.primary.main,
                color: theme.palette.common.white,
                borderRadius: theme.spacing(0.5),
                padding: theme.spacing(0.5, 1),
                "&:hover": {
                  bgcolor: theme.palette.primary.dark,
                },
                alignItems: "center",
                justifyContent: "center",
              }),
              onClick: () => setUploadQuestions(true),
            }}
            title="Upload"
            Icon={AiOutlineCloudUpload}
          />
        </Stack>
      </Stack>
      <Stack padding={2} spacing={2}>
        {questions.results.length <= 0 && <NoDataView />}
        {questions.results.length > 0 &&
          questions.results.map((q, index) => (
            <QuestionInfoCard index={index + 1} question={q} key={q._id} />
          ))}
      </Stack>
    </Stack>
  );
}
