import { Divider, Stack, TableRow, Typography } from "@mui/material";
import React from "react";
import { GrFormNext, GrFormPrevious } from "react-icons/gr";
import {
  CustomIconButton,
  PaginationComponent,
  SearchInput,
} from "../../../components";
import { TableTemplate } from "../../../shared";
import { generateId } from "../../../utils";
import { PageHeader } from "../components";
import { SchoolTableCellHeader } from "../data";

export default function UsersPage() {
  return (
    <Stack spacing={1} overflow="hidden" padding={2} height="100%" width="100%">
      <PageHeader />
      <Stack
        padding={1}
        bgcolor={(theme) => theme.palette.common.white}
        borderRadius={(theme) => theme.spacing(0.5)}
        spacing={1}
      >
        <Stack
          direction="row"
          width="100%"
          justifyContent="space-between"
          alignItems="center"
          overflow="hidden"
        >
          <Stack
            direction="row"
            alignItems="center"
            justifyContent="flex-start"
            paddingLeft={1}
          >
            <Typography
              variant="body1"
              color={(theme) => theme.palette.common.black}
            >
              Users
            </Typography>
            <Typography
              fontSize={(theme) => theme.spacing(1.5)}
              variant="caption"
              color="primary"
              sx={(theme) => ({
                marginLeft: theme.spacing(1),
              })}
            >
              100 users
            </Typography>
          </Stack>
          <SearchInput />
        </Stack>
        <Divider />
        <Stack height="60vh">
          <TableTemplate count={100} header={SchoolTableCellHeader}>
            {Array.from({ length: 100 }).map((_, i) => (
              <TableRow key={generateId()}></TableRow>
            ))}
          </TableTemplate>
        </Stack>
        <Stack
          direction="row"
          alignItems="center"
          justifyContent="space-between"
          padding={(theme) => theme.spacing(1, 0)}
        >
          <CustomIconButton
            variant="outlined"
            Icon={GrFormPrevious}
            title="Previous"
            reverse
          />
          <Stack>
            <PaginationComponent
              page={1}
              handleRowChange={(e) => {}}
              onPageChange={(e) => {}}
              count={20}
              rowsPerPage={5}
            />
          </Stack>
          <CustomIconButton variant="outlined" Icon={GrFormNext} title="Next" />
        </Stack>
      </Stack>
    </Stack>
  );
}

/*

app.use(cors({origin:"*"}))

*/
