import { Divider, Radio, RadioGroup, Stack, Typography } from "@mui/material";
import React, { useState } from "react";
import { CreateQuestionDto } from "../../../model/QuestionsModel";
import { generateId } from "../../../utils";

interface IProps {
  question: CreateQuestionDto;
  index: number;
}
export default function QuestionInfoCard({ question, index }: IProps) {
  const [selectedAnswer, setSelectedAnswer] = useState<string>("");
  return (
    <Stack
      padding={1}
      width="100%"
      boxShadow={(theme) => theme.shadows[1]}
      bgcolor={(theme) => theme.palette.common.white}
    >
      <Stack
        direction="row"
        width="100%"
        alignItems="center"
        justifyContent="flex-start"
        spacing={1}
      >
        <Typography variant="caption">{index.toString()}</Typography>
        <Typography variant="body2">{question.statement}</Typography>
      </Stack>
      <Divider />
      <Stack
        direction="row"
        spacing={1.5}
        alignItems="center"
        justifyContent="flex-start"
      >
        {question.options.map((answer, i) => (
          <Stack key={generateId()}>
            <RadioGroup
              value={selectedAnswer}
              onChange={(e) => setSelectedAnswer(e.target.value)}
              name={question.questionId}
            >
              <Stack
                direction="row"
                alignItems="center"
                justifyContent="flex-start"
                spacing={1}
              >
                <Radio name={question.questionId} value={answer} size="small" />
                <Typography variant="body2">{answer}</Typography>
              </Stack>
            </RadioGroup>
          </Stack>
        ))}
      </Stack>
    </Stack>
  );
}
