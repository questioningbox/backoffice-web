import { Row } from "read-excel-file";
import { ChallengePrice } from "../../../model/ChallegeCategory";
import {
  CreateQuestionDto,
  QuestionInitialInfo,
} from "../../../model/QuestionsModel";
import { generateId } from "../../../utils";

export function prepareQuestionData(row: Row): CreateQuestionDto {
  console.log(row);
  let question: CreateQuestionDto = {
    ...QuestionInitialInfo,
    statement: "",
    questionId: generateId(),
  };
  let questionAnswers: string[] = [];
  let plausibleAnswers: string[] = [];
  question.statement = row[0] ? row[0].toString() : "";
  for (let i = 1; i < 5; i++) {
    row[i] && plausibleAnswers.push(row[i].toString());
  }
  for (let i = 5; i < row.length; i++) {
    row[i] && questionAnswers.push(row[i].toString());
  }

  question.answers = questionAnswers;
  question.options = plausibleAnswers;
  questionAnswers = [];
  plausibleAnswers = [];

  return question;
}

////////
export function ValidateChallengePrice(info: ChallengePrice) {
  if (info.title.length <= 0) {
    throw "Challenge Price Title Is Required";
  }
  if (info.position <= 0) {
    throw "Challenge Price Position Required";
  }
}
