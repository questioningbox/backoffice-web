import React from "react";
import { Route, Routes } from "react-router-dom";
import {
  CompetitionsPage,
  CoursesPage,
  FeaturesPage,
  HomeContentPage,
  HomePage,
  QuestionsPage,
  UsersPage,
} from "../pages/home/view";
import NavigationRoutes from "../routes/NavigationRoutes";

export default function HomeRouter() {
  return (
    <Routes>
      <Route path={NavigationRoutes.home.root} element={<HomePage />}>
        <Route path="" element={<HomeContentPage />} />
        <Route path={NavigationRoutes.home.users} element={<UsersPage />} />
        <Route
          path={NavigationRoutes.home.questions}
          element={<QuestionsPage />}
        />
        <Route path={NavigationRoutes.home.courses} element={<CoursesPage />} />
        <Route
          path={NavigationRoutes.home.features}
          element={<FeaturesPage />}
        />
        <Route
          path={NavigationRoutes.home.competions}
          element={<CompetitionsPage />}
        />
      </Route>
    </Routes>
  );
}
