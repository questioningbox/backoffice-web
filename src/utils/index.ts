import { v4 } from "uuid";

export function generateId(): string {
  const idSections = v4().toString().split("-");
  let id = "";
  idSections.map((p) => {
    id += p;
  });
  return id;
}
