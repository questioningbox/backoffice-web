import { combineReducers } from "@reduxjs/toolkit";

import {
  UserReducer,
  ResponseReducer,
  FeaturesReducer,
  CourseReducer,
  QuestionsReducer,
} from "../features";

///
export default combineReducers({
  UserReducer,
  ResponseReducer,
  FeaturesReducer,
  CourseReducer,
  QuestionsReducer,
});
