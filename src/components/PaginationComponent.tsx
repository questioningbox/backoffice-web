import { TablePagination, TablePaginationProps } from "@mui/material";
import React, { ChangeEvent } from "react";

interface IProps {
  count: number;
  rowsPerPage: number;
  page: number;
  handleRowChange?: (e: ChangeEvent<HTMLInputElement>) => void;
  onPageChange: (e: React.MouseEvent<HTMLButtonElement> | null) => void;
  rowsPerPageOption?: any[];
  props?: TablePaginationProps;
}
export default function PaginationComponent({
  count,
  rowsPerPage,
  rowsPerPageOption,
  handleRowChange,
  onPageChange,
  page,
  props,
}: IProps) {
  return (
    <TablePagination
      rowsPerPageOptions={[5, 10, 25]}
      component="div"
      count={count}
      rowsPerPage={rowsPerPage}
      page={page}
      showLastButton={false}
      showFirstButton={false}
      nextIconButtonProps={{ hidden: true }}
      backIconButtonProps={{ hidden: true }}
      onPageChange={(e) => onPageChange(e)}
      onRowsPerPageChange={handleRowChange}
      {...props}
    />
  );
}
